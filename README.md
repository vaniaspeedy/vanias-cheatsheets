# Vania's Cheatsheets [v1.0]
***
`Vania's Cheatsheets` is a collection of various study guides and resources, compiled personally by Vania Smirnov. Most of these are original works, with some reference materials thrown in that are not.

Note: If you own the rights to a document in this repository, please contact me with proof of ownership and a signed request for removal, with which I will gladly comply. 

#### File List

See filelist.txt


#### License

`Vania's Cheatsheets` is uncopyrighted. I believe knowledge should be free, so distribute this work as much as you want. Be nice and give credit where credit is due.

#### Contributors
Vania Smirnov (vaniaspeedy)
